package etr.model.bean;

public class Kurzus {
	private String kurzuskod;//10
	private String o_eha;//7
	private String teremkod;//7
	private String idopont;//10
	private int letszam;
	private int kredit;

	public String getKurzuskod() {
		return kurzuskod;
	}
	public void setKurzuskod(String kurzuskod) {
		this.kurzuskod = kurzuskod;
	}
	public String getO_eha() {
		return o_eha;
	}
	public void setO_eha(String o_eha) {
		this.o_eha = o_eha;
	}
	public String getTeremkod() {
		return teremkod;
	}
	public void setTeremkod(String teremkod) {
		this.teremkod = teremkod;
	}
	public String getIdopont() {
		return idopont;
	}
	public void setIdopont(String idopont) {
		this.idopont = idopont;
	}
	public int getLetszam() {
		return letszam;
	}
	public void setLetszam(int letszam) {
		this.letszam = letszam;
	}
	public int getKredit() {
		return kredit;
	}
	public void setKredit(int kredit) {
		this.kredit = kredit;
	}


	
	
	
}
