package etr.model.bean;

public class Terem {
	private String teremkod;
	private String epulet_neve;
	private String epulet_cime;
	private int ferohely;

	public String getTeremkod() {
		return teremkod;
	}
	public void setTeremkod(String teremkod) {
		this.teremkod = teremkod;
	}
	public String getEpulet_neve() {
		return epulet_neve;
	}
	public void setEpulet_neve(String epulet_neve) {
		this.epulet_neve = epulet_neve;
	}
	public String getEpulet_cime() {
		return epulet_cime;
	}
	public void setEpulet_cime(String epulet_cime) {
		this.epulet_cime = epulet_cime;
	}
	public int getFerohely() {
		return ferohely;
	}
	public void setFerohely(int ferohely) {
		this.ferohely = ferohely;
	}
	
	

	

}
