package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import etr.model.bean.Oktato;
import etr.view.EtrGUI;
import etr.view.Labels;

public class AddOktatoDialog extends JDialog implements ActionListener{

	private EtrGUI gui;
	private JTextField o_eha = new JTextField(11);
	private JTextField o_nev = new JTextField(20);
	private JTextField o_jelszo = new JTextField(20);
	
	private JButton okButton = new JButton(Labels.OK);
	private JButton cancelButton = new JButton(Labels.CANCEL);
	
	public AddOktatoDialog(EtrGUI gui){
		super(gui, true);
		
		this.gui = gui;
		
		setTitle(Labels.OKTATO_FELVETEL);
		setLocation(300, 130);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(3,2));
		inputPanel.add(new JLabel(Labels.OKTATO_EHA));
		inputPanel.add(o_eha);
		inputPanel.add(new JLabel(Labels.OKTATO_NEV));
		inputPanel.add(o_nev);
		inputPanel.add(new JLabel(Labels.OKTATO_JELSZO));
		inputPanel.add(o_jelszo);	
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);
		okButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		cancelButton.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		
		setVisible(true);
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton){
			if(o_nev.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.OKTATO_NEV_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(o_eha.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.OKTATO_EHA_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(o_jelszo.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.OKTATO_JELSZO_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			Oktato oktato = new Oktato();
			oktato.setO_nev(o_nev.getText());
			oktato.setO_eha(o_eha.getText());
			oktato.setO_jelszo(o_jelszo.getText());
			
			if(gui.getController().addOktato(oktato)){
				setVisible(false);
			} else {
				JOptionPane.showMessageDialog(
						gui, 
						Labels.OKTATO_LETEZIK,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} else if(e.getSource() == cancelButton){
			setVisible(false);
		}
		
		
	}
	
}
