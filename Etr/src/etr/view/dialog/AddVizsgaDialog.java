package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import etr.model.bean.Vizsga;
import etr.view.EtrGUI;
import etr.view.Labels;

public class AddVizsgaDialog extends JDialog implements ActionListener{

	private EtrGUI gui;
	private JSpinner az = new JSpinner();
	private JSpinner oaz = new JSpinner();
	private JSpinner jegy = new JSpinner();
	private JButton okButton = new JButton(Labels.OK);
	private JButton cancelButton = new JButton(Labels.CANCEL);
	
	
	public AddVizsgaDialog(EtrGUI gui){
		super(gui, true);
		
		this.gui = gui;
		
		setTitle(Labels.VIZSGA_FELVETEL);
		setLocation(300, 130);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(3,2));
		inputPanel.add(new JLabel(Labels.VIZSGA_AZ));
		inputPanel.add(az);
		inputPanel.add(new JLabel(Labels.VIZSGA_OAZ));
		inputPanel.add(oaz);
		inputPanel.add(new JLabel(Labels.VIZSGA_JEGY));
		inputPanel.add(jegy);
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);
		okButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		cancelButton.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		
		setVisible(true);
	
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton){
			
			Vizsga vizsga = new Vizsga();
			vizsga.setAz((Integer)az.getValue());
			vizsga.setOaz((Integer)oaz.getValue());
			vizsga.setJegy((Integer)jegy.getValue());
			
			
			if(gui.getController().addVizsga(vizsga)){
				setVisible(false);
			} else {
				JOptionPane.showMessageDialog(
						gui, 
						Labels.VIZSGA_LETEZIK,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} else if(e.getSource() == cancelButton){
			setVisible(false);
		}
		
	}
	
}