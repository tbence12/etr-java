package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import etr.controller.EtrController;
import etr.model.bean.Kurzus;
import etr.model.bean.Oktato;
import etr.model.bean.Vizsga;
import etr.view.OktatoGUI;

public class VizsgaErtekelesDialog extends JDialog implements ActionListener{
	private OktatoGUI gui;
	private JButton ertekelButton = new JButton("�rt�kel");
	private int vaz = 0;
	private JSpinner jegys = new JSpinner();
	
	EtrController controller = new EtrController();
	Oktato oktato = new Oktato();
	
	
	public VizsgaErtekelesDialog(OktatoGUI gui, Oktato oktato){
		super(gui, true);
		
		this.gui = gui;
		this.oktato = oktato;
		
		int i = 0;
		String [] hStrings = new String[1];
		for(Vizsga vizsga : controller.sajatOVizsga(oktato.getO_eha())){				
			String az = Integer.toString(vizsga.getAz());	
			String oaz = Integer.toString(vizsga.getOaz());
			String jegy = Integer.toString(vizsga.getJegy());
			hStrings[i] = "   " + az + "  |  " + oaz + "  |  " + jegy + "   ";
			hStrings = Arrays.copyOf(hStrings, hStrings.length+1);
			i++;		
		}
		
		JComboBox hList = new JComboBox(hStrings);
        hList.setSelectedIndex(i);
        hList.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	JComboBox cb = (JComboBox)e.getSource();
                String vizsgasor = (String)cb.getSelectedItem();
                char[] kurzussor_arr = vizsgasor.toCharArray();
                vaz =  (kurzussor_arr[3]-'0') *1000 + (kurzussor_arr[4]-'0')*100 + (kurzussor_arr[5]-'0')*10 + (kurzussor_arr[6]-'0');             
                System.out.println("a vizsgaazonos�t�: " + vaz);

            }
        });
        
        JPanel panelSz = new JPanel();
        panelSz.setLayout(new BoxLayout(panelSz, BoxLayout.PAGE_AXIS));
        panelSz.add(new JLabel("  V�laszd ki a vizsg�k k�z�l, hogy melyiket szeretn�d �rt�kelni!"));
        panelSz.add(new JLabel(" "));
        panelSz.add(new JLabel(" "));
        panelSz.add(new JLabel(" "));
        panelSz.add(new JLabel("                      Azonos�t�  |  �raazonos�t�  |  Jegy"));
        panelSz.setBackground( Color.decode("#79B3FA"));
        
        JPanel panelk = new JPanel();
        panelk.setLayout(new FlowLayout(FlowLayout.CENTER));
        panelk.add(panelSz);
        panelk.setBackground( Color.decode("#79B3FA"));
        
        JPanel panelCb = new JPanel();
        panelCb.setLayout(new BoxLayout(panelCb, BoxLayout.PAGE_AXIS));
        panelCb.add(hList);
        panelCb.add(new JLabel(" "));
        panelCb.add(new JLabel(" "));
        panelCb.add(new JLabel("�rdem jegy:"));
        panelCb.add(jegys);
        panelCb.add(new JLabel(" "));
        panelCb.add(new JLabel(" "));
        panelCb.add(ertekelButton);
        ertekelButton.addActionListener(this);
        panelCb.setBackground( Color.decode("#79B3FA"));
        
        JPanel panelB = new JPanel();
        panelB.setLayout(new FlowLayout(FlowLayout.CENTER));
        panelB.add(panelCb);
        panelB.setBackground( Color.decode("#79B3FA"));
        
        //JFrame frame = new JFrame();
        
        setLayout(new BorderLayout());
        	add(panelk, BorderLayout.NORTH);
	//	    frame.add(panelCb, BorderLayout.CENTER);
		    add(panelB, BorderLayout.CENTER);
		    setTitle("Vizsga �rt�kel�s");
		    setSize(600, 300);
		    setLocation(300, 130);
		    setVisible(true);	
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == ertekelButton){
			int jegy = (Integer)jegys.getValue();
			if(jegy > 0 && jegy < 6){
			controller.ertekelVizsga(vaz, jegy);
			} else {
				JOptionPane.showMessageDialog(
						null, 
						"�rv�nytelen jegy",
						"Hiba",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
