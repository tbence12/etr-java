package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import etr.controller.EtrController;
import etr.model.bean.Hallgato;
import etr.model.bean.Kurzus;
import etr.model.bean.Oktato;
import etr.model.bean.Terem;
import etr.model.bean.Vizsga;
import etr.view.HallgatoGUI;
import etr.view.Labels;

public class KeresDialog extends JFrame implements ActionListener{
	private HallgatoGUI gui;
	private JTextField hkeresszoveg = new JTextField(20);
	private JTextField okeresszoveg = new JTextField(20);
	private JTextField kkeresszoveg = new JTextField(20);
	private JTextField tkeresszoveg = new JTextField(20);
	private JTextField vkeresszoveg = new JTextField(20);
	private JButton hkeresesButton = new JButton("Keres�s");
	private JButton okeresesButton = new JButton("Keres�s");
	private JButton kkeresesButton = new JButton("Keres�s");
	private JButton tkeresesButton = new JButton("Keres�s");
	private JButton vkeresesButton = new JButton("Keres�s");
	
	private String[] hStrings = { "h_eha", "h_nev", "kezdesi_ev", "telj_kredit", "evfolyam" };
	private String[] oStrings = { "o_eha", "o_nev" };
	private String[] kStrings = { "kurzuskod","o_eha", "teremkod", "idopont", "letszam", "kredit" };
	private String[] tStrings = { "teremkod", "epulet_neve", "epulet_cime", "ferohely" };
	private JComboBox hList = new JComboBox(hStrings);
	private JComboBox oList = new JComboBox(oStrings);
	private JComboBox kList = new JComboBox(kStrings);
	private JComboBox tList = new JComboBox(tStrings);
	private String holKeres = "";
	private String mitKeres = "";
	
	final JTextField result1 = new JTextField();
	final JTextField result2 = new JTextField();
	final JTextField result3 = new JTextField();
	final JTextField result4 = new JTextField();
	
	EtrController controller = new EtrController();
	
		public KeresDialog(HallgatoGUI gui){
			//super(gui, true);			
			this.gui = gui;
	 
	        //Create the combo box, select the item at index 4.
	        //Indices start at 0, so 4 specifies the pig.
	        
	        hList.setSelectedIndex(4);
	        hList.addActionListener( new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    JComboBox combo = (JComboBox)e.getSource();
                    holKeres = (String)combo.getSelectedItem();
                    result1.setText(holKeres);
                }
            });
	        oList.setSelectedIndex(1);
	        oList.addActionListener( new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    JComboBox combo = (JComboBox)e.getSource();
                    holKeres = (String)combo.getSelectedItem();
                    result2.setText(holKeres);
                }
            });
	        kList.setSelectedIndex(5);
	        kList.addActionListener( new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    JComboBox combo = (JComboBox)e.getSource();
                    holKeres = (String)combo.getSelectedItem();
                    result3.setText(holKeres);
                }
            });
	        tList.setSelectedIndex(3);
	        tList.addActionListener( new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    JComboBox combo = (JComboBox)e.getSource();
                    holKeres = (String)combo.getSelectedItem();
                    result4.setText(holKeres);
                }
            });
	        
			JTabbedPane tabbedPane = new JTabbedPane();
		 	
			JPanel tab1 = new JPanel();
	        tab1.add(new JLabel("V�laszd ki, hogy mire szeretn�l keresni: "));
			tab1.add(hList);
			tab1.add(new JLabel("�rd ide amit keresni szeretn�l: "));
			tab1.add(hkeresszoveg);
			hkeresszoveg.addActionListener(this);
			tab1.add(new JLabel(Labels.EMPTY));
			tab1.add(result1);
			JLabel katt1 = new JLabel("Katt ide!    ");
			katt1.setForeground(Color.RED);;
			tab1.add(katt1);
			tab1.add(new JLabel("ut�na"));
			tab1.add(hkeresesButton);
			hkeresesButton.addActionListener(this);
			tab1.setBackground( Color.decode("#537552"));	
					
	        JPanel tab2 = new JPanel();
	        tab2.add(new JLabel("V�laszd ki, hogy mire szeretn�l keresni: "));
			tab2.add(oList);
			tab2.add(new JLabel("�rd ide amit keresni szeretn�l: "));
			tab2.add(okeresszoveg);
			okeresszoveg.addActionListener(this);
			tab2.add(new JLabel(Labels.EMPTY));
			tab2.add(result2);
			JLabel katt2 = new JLabel("Katt ide!    ");
			katt2.setForeground(Color.RED);;
			tab2.add(katt2);
			tab2.add(new JLabel("ut�na"));
			tab2.add(okeresesButton);
			okeresesButton.addActionListener(this);
			tab2.setBackground( Color.decode("#348133"));
	 
	        JPanel tab3 = new JPanel();
	        tab3.add(new JLabel("V�laszd ki, hogy mire szeretn�l keresni: "));
			tab3.add(kList);
			tab3.add(new JLabel("�rd ide amit keresni szeretn�l: "));
			tab3.add(kkeresszoveg);
			kkeresszoveg.addActionListener(this);
			tab3.add(new JLabel(Labels.EMPTY));
			tab3.add(result3);
			JLabel katt3 = new JLabel("Katt ide!    ");
			katt3.setForeground(Color.RED);;
			tab3.add(katt3);
			tab3.add(new JLabel("ut�na"));
			tab3.add(kkeresesButton);
			kkeresesButton.addActionListener(this);
			tab3.setBackground( Color.decode("#347233"));
			
	        JPanel tab4 = new JPanel();
	        tab4.add(new JLabel("V�laszd ki, hogy mire szeretn�l keresni: "));
			tab4.add(tList);
			tab4.add(new JLabel("�rd ide amit keresni szeretn�l: "));
			tab4.add(tkeresszoveg);
			tkeresszoveg.addActionListener(this);
			tab4.add(new JLabel(Labels.EMPTY));
			tab4.add(result4);
			JLabel katt4 = new JLabel("Katt ide!    ");
			katt4.setForeground(Color.RED);;
			tab4.add(katt4);
			tab4.add(new JLabel("ut�na"));
			tab4.add(tkeresesButton);
			tkeresesButton.addActionListener(this);
			tab4.setBackground( Color.decode("#226621"));
	 
	        tabbedPane.addTab("Hallgat� keres�s", tab1);
	        tabbedPane.addTab("Oktat� keres�s", tab2);
	        tabbedPane.addTab("Kurzus keres�s", tab3);
	        tabbedPane.addTab("Terem keres�s", tab4);
	        
	        setLayout(new BorderLayout());
		    	
			    add(tabbedPane, BorderLayout.CENTER);
			    setTitle("Keres�s");
			    setSize(750, 500);
			    setLocation(300, 130);
			    setVisible(true);
		    
		}
		
	

			@Override
			public void actionPerformed(ActionEvent e) {
		        if(e.getSource() == hkeresesButton){	        
		        mitKeres = hkeresszoveg.getText();
		        System.out.println("Hol: " + holKeres + ", Mit: " + mitKeres);
		        
		        
				String[][] sor= new String[50][10];
				String[] oszlop = {"Hallgat� EHA","Hallgat� neve","Kezd�si �v",
						"Jelenlegi kreditsz�m","Megszerzett kreditsz�m","�vfolyam"};
				int i = 0;
				for(Hallgato hallgato : controller.listHallgatoKeres(mitKeres, holKeres)){
				
					String[] hozza = new String[7] ;
					String h_eha = hallgato.getH_eha();
					String h_nev = hallgato.getH_nev();
					String h_jelszo = hallgato.getH_jelszo();
					String kezdesi_ev = Integer.toString(hallgato.getKezdesi_ev());
					String akt_kreditszam = Integer.toString(hallgato.getAkt_kreditszam());
					String telj_kreditszam = Integer.toString(hallgato.getTelj_kreditszam());
					String evfolyam = Integer.toString(hallgato.getEvfolyam());
					String tartozas = Integer.toString(hallgato.getTartozas());
									
				
					hozza[0] = h_eha;
					hozza[1] = h_nev;
					hozza[2] = kezdesi_ev;
					hozza[3] = akt_kreditszam;
					hozza[4] = telj_kreditszam;
					hozza[5] = evfolyam;
					hozza[6] = tartozas;
					
					for(int j=0;j<7;j++){
						sor[i][j]= hozza[j];
					}
					i++;
						
				}
				
				JFrame frame = new JFrame();
			    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			    
				JTable table = new JTable(sor,oszlop);
				
				 JScrollPane scrollPane = new JScrollPane(table);
				    frame.add(scrollPane, BorderLayout.CENTER);
				    frame.setTitle("Hallgat�k keres�si eredm�nye");
				    frame.setSize(750, 500);
				    frame.setLocation(300, 130);
				    frame.setVisible(true);

		        }
		        if(e.getSource() == okeresesButton){	        
			        mitKeres = okeresszoveg.getText();
			        
			        String[][] sor= new String[50][10];
					String[] oszlop = {"Oktat� EHA","Oktat� neve"};
					int i = 0;
					
					for(Oktato oktato : controller.listOktatoKeres(mitKeres, holKeres)){		
						/*System.out.println(oktato.getO_eha() + " " + oktato.getO_nev()
						+ " " + oktato.getO_jelszo());*/
						
						String[] hozza = new String[3] ;
						String o_eha = oktato.getO_eha();
						String o_nev = oktato.getO_nev();
						String o_jelszo = oktato.getO_jelszo();								
					
						hozza[0] = o_eha;
						hozza[1] = o_nev;
						
						
						for(int j=0;j<2;j++){
							sor[i][j]= hozza[j];
						}
						i++;
								
					}
					
					JFrame frame = new JFrame();
				    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				    
					JTable table = new JTable(sor,oszlop);
					
					 JScrollPane scrollPane = new JScrollPane(table);
					    frame.add(scrollPane, BorderLayout.CENTER);
					    frame.setTitle("Oktat�k keres�si eredm�nye");
					    frame.setSize(750, 500);
					    frame.setLocation(300, 130);
					    frame.setVisible(true);
					
				}
		        if(e.getSource() == kkeresesButton){	        
			        mitKeres = kkeresszoveg.getText();
			        
			        String[][] sor= new String[50][10];
					String[] oszlop = {"Kurzusk�d","Oktat� EHA","Teremk�d","Id�pont","L�tsz�m","Kredit"};
					int i = 0;
					for(Kurzus kurzus : controller.listKurzusKeres(mitKeres, holKeres)){	
						
						String[] hozza = new String[6] ;
						String kurzuskod = kurzus.getKurzuskod();
						String o_eha = kurzus.getO_eha();
						String teremkod = kurzus.getTeremkod();
						String idopont = kurzus.getIdopont();
						String letszam = Integer.toString(kurzus.getLetszam());	
						String kredit = Integer.toString(kurzus.getKredit());	
					
						hozza[0] = kurzuskod;
						hozza[1] = o_eha;
						hozza[2] = teremkod;
						hozza[3] = idopont;
						hozza[4] = letszam;
						hozza[5] = kredit;
						
						for(int j=0;j<6;j++){
							sor[i][j]= hozza[j];
						}
						i++;
								
					}
					
					JFrame frame = new JFrame();
				    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				    
					JTable table = new JTable(sor,oszlop);
					
					 JScrollPane scrollPane = new JScrollPane(table);
					    frame.add(scrollPane, BorderLayout.CENTER);
					    frame.setTitle("Kurzusok keres�si eredm�nye");
					    frame.setSize(750, 500);
					    frame.setLocation(300, 130);
					    frame.setVisible(true);
			        
				}
		        if(e.getSource() == tkeresesButton){	        
			        mitKeres = tkeresszoveg.getText();
			        
			        String[][] sor= new String[50][10];
					String[] oszlop = {"Teremk�d","Ep�let neve","�p�let c�me","F�r�hely"};
					int i = 0;
					for(Terem terem : controller.listTeremKeres(mitKeres, holKeres)){		
						
						String[] hozza = new String[4] ;
						String teremkod = terem.getTeremkod();
						String epulet_neve = terem.getEpulet_neve();
						String epulet_cime = terem.getEpulet_cime();
						String ferohely = Integer.toString(terem.getFerohely());					
					
						hozza[0] = teremkod;
						hozza[1] = epulet_neve;
						hozza[2] = epulet_cime;
						hozza[3] = ferohely;
						
						for(int j=0;j<4;j++){
							sor[i][j]= hozza[j];
						}
						i++;
								
					}
					
					JFrame frame = new JFrame();
				    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				    
					JTable table = new JTable(sor,oszlop);
					
					 JScrollPane scrollPane = new JScrollPane(table);
					    frame.add(scrollPane, BorderLayout.CENTER);
					    frame.setTitle("Termek keres�si eredm�nye");
					    frame.setSize(750, 500);
					    frame.setLocation(300, 130);
					    frame.setVisible(true);
					
			        
				}
		        
			}
			
			
			
		
	
}
