package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import etr.model.bean.Hallgato;
import etr.view.EtrGUI;
import etr.view.Labels;

public class AddHallgatoDialog extends JDialog implements ActionListener{

	private EtrGUI gui;
	private JTextField h_eha = new JTextField(11);
	private JTextField h_nev = new JTextField(20);
	private JTextField h_jelszo = new JTextField(20);
	private JSpinner kezdesi_ev = new JSpinner();
	private JSpinner akt_kreditszam = new JSpinner();
	private JSpinner telj_kreditszam = new JSpinner();
	private JSpinner tartozas = new JSpinner();
	private JSpinner evfolyam = new JSpinner();
	private JButton okButton = new JButton(Labels.OK);
	private JButton cancelButton = new JButton(Labels.CANCEL);
	
	
	
	public AddHallgatoDialog(EtrGUI gui){
		super(gui, true);
		
		this.gui = gui;
		
		setTitle(Labels.HALLGATO_FELVETEL);
		setLocation(300, 130);
		
		
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(8,2));
		inputPanel.add(new JLabel(Labels.HALLGATO_EHA));
		inputPanel.add(h_eha);
		inputPanel.add(new JLabel(Labels.HALLGATO_NEV));
		inputPanel.add(h_nev);
		inputPanel.add(new JLabel(Labels.HALLGATO_JELSZO));
		inputPanel.add(h_jelszo);
		inputPanel.add(new JLabel(Labels.HALLGATO_KEZDESI_EV));
		inputPanel.add(kezdesi_ev);
		inputPanel.add(new JLabel(Labels.HALLGATO_AKT_KREDITSZAM));
		inputPanel.add(akt_kreditszam);
		inputPanel.add(new JLabel(Labels.HALLGATO_TELJ_KREDITSZAM));
		inputPanel.add(telj_kreditszam);
		inputPanel.add(new JLabel(Labels.HALLGATO_EVFOLYAM));
		inputPanel.add(evfolyam);
		inputPanel.add(new JLabel(Labels.HALLGATO_TARTOZAS));
		inputPanel.add(tartozas);
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);
		okButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		cancelButton.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		
		setVisible(true);
	
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton){
			if(h_nev.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.HALLGATO_NEV_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(h_eha.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.HALLGATO_EHA_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(h_jelszo.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.HALLGATO_JELSZO_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			Hallgato hallgato = new Hallgato();
			hallgato.setH_nev(h_nev.getText());
			hallgato.setH_eha(h_eha.getText());
			hallgato.setH_jelszo(h_jelszo.getText());
			hallgato.setKezdesi_ev((Integer)kezdesi_ev.getValue());
			hallgato.setAkt_kreditszam((Integer)akt_kreditszam.getValue());
			hallgato.setTelj_kreditszam((Integer)telj_kreditszam.getValue());
			hallgato.setEvfolyam((Integer)evfolyam.getValue());
			hallgato.setTartozas((Integer)tartozas.getValue());
			
			if(gui.getController().addHallgato(hallgato)){
				setVisible(false);
			} else {
				JOptionPane.showMessageDialog(
						gui, 
						Labels.HALLGATO_LETEZIK,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} else if(e.getSource() == cancelButton){
			setVisible(false);
		}
		
	}
	
}
