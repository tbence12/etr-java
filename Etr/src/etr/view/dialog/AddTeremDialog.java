package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import etr.model.bean.Terem;
import etr.view.EtrGUI;
import etr.view.Labels;

public class AddTeremDialog extends JDialog implements ActionListener{

	private EtrGUI gui;
	private JTextField teremkod = new JTextField(7);
	private JTextField epulet_neve = new JTextField(20);
	private JTextField epulet_cime = new JTextField(20);
	private JSpinner ferohely = new JSpinner();
	private JButton okButton = new JButton(Labels.OK);
	private JButton cancelButton = new JButton(Labels.CANCEL);
	
	
	public AddTeremDialog(EtrGUI gui){
		super(gui, true);
		
		this.gui = gui;
		
		setTitle(Labels.TEREM_FELVETEL);
		setLocation(300, 130);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(7,2));
		inputPanel.add(new JLabel(Labels.TEREM_TEREMKOD));
		inputPanel.add(teremkod);
		inputPanel.add(new JLabel(Labels.TEREM_EPULET_NEVE));
		inputPanel.add(epulet_neve);
		inputPanel.add(new JLabel(Labels.TEREM_EPULET_CIME));
		inputPanel.add(epulet_cime);
		inputPanel.add(new JLabel(Labels.TEREM_FEROHELY));
		inputPanel.add(ferohely);
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);
		okButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		cancelButton.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		
		setVisible(true);
	
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton){
			if(teremkod.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.TEREM_TEREMKOD_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(epulet_neve.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.TEREM_EPULET_NEVE_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(epulet_cime.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.TEREM_EPULET_CIME_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			Terem terem = new Terem();
			terem.setTeremkod(teremkod.getText());
			terem.setEpulet_neve(epulet_neve.getText());
			terem.setEpulet_cime(epulet_cime.getText());
			terem.setFerohely((Integer)ferohely.getValue());
			
			if(gui.getController().addTerem(terem)){
				setVisible(false);
			} else {
				JOptionPane.showMessageDialog(
						gui, 
						Labels.TEREM_LETEZIK,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} else if(e.getSource() == cancelButton){
			setVisible(false);
		}
		
	}
	
}