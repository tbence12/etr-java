package etr.view.dialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import etr.model.bean.Kurzus;
import etr.view.EtrGUI;
import etr.view.Labels;

public class AddKurzusDialog extends JDialog implements ActionListener{

	private EtrGUI gui;
	private JTextField kurzuskod = new JTextField(10);
	private JTextField o_eha = new JTextField(11);
	private JTextField teremkod = new JTextField(7);
	private JTextField idopont = new JTextField(10);
	private JSpinner letszam = new JSpinner();
	private JSpinner kredit = new JSpinner();
	
	private JButton okButton = new JButton(Labels.OK);
	private JButton cancelButton = new JButton(Labels.CANCEL);
	
	public AddKurzusDialog(EtrGUI gui){
		super(gui, true);
		
		this.gui = gui;
		
		setTitle(Labels.KURZUS_FELVETEL);
		setLocation(300, 130);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(6,2));
		inputPanel.add(new JLabel(Labels.KURZUS_KURZUSKOD));
		inputPanel.add(kurzuskod);
		inputPanel.add(new JLabel(Labels.KURZUS_O_EHA));
		inputPanel.add(o_eha);
		inputPanel.add(new JLabel(Labels.KURZUS_TEREMKOD));
		inputPanel.add(teremkod);
		inputPanel.add(new JLabel(Labels.KURZUS_IDOPONT));
		inputPanel.add(idopont);
		inputPanel.add(new JLabel(Labels.KURZUS_LETSZAM));
		inputPanel.add(letszam);	
		inputPanel.add(new JLabel(Labels.KURZUS_KREDIT));
		inputPanel.add(kredit);

		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);
		okButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		cancelButton.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		
		setVisible(true);
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton){
			if(kurzuskod.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.KURZUS_KURZUSKOD_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(o_eha.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.KURZUS_O_EHA_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(teremkod.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.KURZUS_TEREMKOD_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(idopont.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						gui, 
						Labels.KURZUS_IDOPONT_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			Kurzus kurzus = new Kurzus();
			kurzus.setKurzuskod(kurzuskod.getText());
			kurzus.setO_eha(o_eha.getText());
			kurzus.setTeremkod(teremkod.getText());
			kurzus.setIdopont(idopont.getText());
			kurzus.setLetszam((Integer)letszam.getValue());
			kurzus.setKredit((Integer)kredit.getValue());
			
			if(gui.getController().addKurzus(kurzus)){
				setVisible(false);
			} else {
				JOptionPane.showMessageDialog(
						gui, 
						Labels.KURZUS_LETEZIK,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} else if(e.getSource() == cancelButton){
			setVisible(false);
		}
		
		
	}
	
}
