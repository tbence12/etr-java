package etr.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import etr.controller.EtrController;
import etr.model.bean.Belep;
import etr.model.bean.Hallgato;
import etr.model.bean.Kurzus;
import etr.model.bean.Oktato;

public class EtrBelep extends JFrame implements ActionListener {
	private JTextField eha = new JTextField(11);
	private JPasswordField jelszo = new JPasswordField(20);
	private JButton okButton = new JButton(Labels.BELEP);
	private JButton cancelButton = new JButton(Labels.KILEP);
	
	
	
	EtrController controller = new EtrController();
	
	
	public EtrBelep(EtrController controller){
		this.controller=controller;
		setTitle("Etr Belépés");
		setLocation(800, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBackground( Color.decode("#2392E8") );
		inputPanel.setLayout(new GridLayout(2,2));
		inputPanel.add(new JLabel(Labels.BELEP_EHA));
		eha.setBackground(Color.YELLOW);
		eha.setCaretColor(Color.RED);
		eha.setBorder(null);
		inputPanel.add(eha);
		inputPanel.add(new JLabel(Labels.BELEP_JELSZO));
		jelszo.setBackground(Color.decode("#E739A4"));
		jelszo.setCaretColor(Color.GREEN);
		jelszo.setBorder(null);
		
		inputPanel.add(jelszo);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground( Color.decode("#4ADB97"));
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		buttonPanel.add(okButton);
		okButton.setBackground( Color.decode("#E5972B"));
		okButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		cancelButton.setBackground( Color.decode("#E5402B"));
		cancelButton.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(inputPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		

		pack();
		
		
		setVisible(true);
	}
	
	public EtrController getController(){
		return controller;
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton){
			if(eha.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						null, 
						Labels.BELEP_EHA_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(jelszo.getText().isEmpty()){
				JOptionPane.showMessageDialog(
						null, 
						Labels.BELEP_JELSZO_KOTELEZO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			Belep belep = new Belep();
			belep.setEha(eha.getText());
			belep.setJelszo(new String(jelszo.getPassword()));
			
			char[] eha_arr = eha.getText().toCharArray();
			String uj_jelszo = new String(jelszo.getPassword()); 
			
			if(eha_arr[0]=='C'){	//Referens
				System.out.println(eha_arr[0]);
				controller.startReferens();
				try(  PrintWriter out = new PrintWriter( new FileWriter("bejelentkezesek.txt",true) )  ){
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
					Date date = new Date();
					//logWriter = new BufferedWriter(new FileWriter(dateFormat.format(date) + " serverLog.log", true));
					out.println( dateFormat.format(date) + " - " + eha.getText() + " Referens bejelentkezett." );
				    out.close();
				} catch (Exception f) {
					System.out.println("Sikertelen bejelentkezés listázás.");
					f.printStackTrace();
				}	
				setVisible(false);
				return;
			}
			if(eha_arr[0]=='A'){	//Hallgató
				Hallgato hallgato = new Hallgato();
				/*hallgato.setH_eha(belep.getEha());
				hallgato.setH_jelszo(belep.getJelszo());*/
				hallgato = controller.ellHallgato(belep);
				controller.startHallgato(hallgato);
				try(  PrintWriter out = new PrintWriter( new FileWriter("bejelentkezesek.txt",true) )  ){
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
					Date date = new Date();
					//logWriter = new BufferedWriter(new FileWriter(dateFormat.format(date) + " serverLog.log", true));
					out.println( dateFormat.format(date) + " - " + eha.getText() + " Referens bejelentkezett." );
				    out.close();
				} catch (Exception f) {
					System.out.println("Sikertelen bejelentkezés listázás.");
					f.printStackTrace();
				}	
					
				setVisible(false);
				return;
			}if(eha_arr[0]=='h'){
				Hallgato hallgato = new Hallgato();
				controller.startHallgato(hallgato);
				setVisible(false);
				return;
			}
			if(eha_arr[0]=='B'){	//Oktató
				Oktato oktato = new Oktato();
				oktato = controller.ellOktato(belep);
				controller.startOktato(oktato);
				try(  PrintWriter out = new PrintWriter( new FileWriter("bejelentkezesek.txt",true) )  ){
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
					Date date = new Date();
					//logWriter = new BufferedWriter(new FileWriter(dateFormat.format(date) + " serverLog.log", true));
					out.println( dateFormat.format(date) + " - " + eha.getText() + " Referens bejelentkezett." );
				    out.close();
				} catch (Exception f) {
					System.out.println("Sikertelen bejelentkezés listázás.");
					f.printStackTrace();
				}	
					
				setVisible(false);
				
			} else {
				JOptionPane.showMessageDialog(
						null, 
						Labels.BELEP_NEM_JO,
						Labels.ERROR,
						JOptionPane.ERROR_MESSAGE);
			}
		} else if(e.getSource() == cancelButton){
			//setVisible(false);
			dispose();
		}
		
		
		
			
	}
	
	

}
