package etr.view;

import etr.controller.EtrController;
import etr.model.bean.Hallgato;
import etr.model.bean.Kurzus;
import etr.model.bean.Oktato;
import etr.model.bean.Terem;
import etr.model.bean.Vizsga;
import etr.view.dialog.AddHallgatoDialog;
import etr.view.dialog.AddKurzusDialog;
import etr.view.dialog.AddOktatoDialog;
import etr.view.dialog.AddTeremDialog;
import etr.view.dialog.AddVizsgaDialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class EtrGUI extends JFrame implements ActionListener{

	private JMenuItem addEtrMenuItem1 = new JMenuItem(Labels.HALLGATO_FELVETEL);
	private JMenuItem listEtrMenuItem1 = new JMenuItem(Labels.HALLGATO_LISTAZ);
	private JMenuItem addEtrMenuItem2 = new JMenuItem(Labels.OKTATO_FELVETEL);
	private JMenuItem listEtrMenuItem2 = new JMenuItem(Labels.OKTATO_LISTAZ);
	private JMenuItem addEtrMenuItem3 = new JMenuItem(Labels.KURZUS_FELVETEL);
	private JMenuItem listEtrMenuItem3 = new JMenuItem(Labels.KURZUS_LISTAZ);
	private JMenuItem addEtrMenuItem4 = new JMenuItem(Labels.TEREM_FELVETEL);
	private JMenuItem listEtrMenuItem4 = new JMenuItem(Labels.TEREM_LISTAZ);
	private JMenuItem addEtrMenuItem5 = new JMenuItem(Labels.VIZSGA_FELVETEL);
	private JMenuItem listEtrMenuItem5 = new JMenuItem(Labels.VIZSGA_LISTAZ);
	private JButton kijelentkezes = new JButton("Kijelentkez�s");
	
	EtrController controller = new EtrController();
	
	public EtrGUI(EtrController controller){
		this.controller=controller;
		setTitle("ETR"); 
		setSize(700,500);
		setLocation(300, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground( Color.DARK_GRAY );
		add(new JLabel(new ImageIcon("etr1.jpg")));

		
		JMenuBar menuBar = new JMenuBar();
		
		//Elso menu
		JMenu menu1 = new JMenu(Labels.R_MENU_ELSO);
		menu1.add(addEtrMenuItem1);
		addEtrMenuItem1.addActionListener(this);
		menu1.add(listEtrMenuItem1);
		listEtrMenuItem1.addActionListener(this);
		menuBar.add(menu1);
		
		//Masodik menu	
		JMenu menu2 = new JMenu(Labels.R_MENU_MASODIK);
		menu2.add(addEtrMenuItem2);
		addEtrMenuItem2.addActionListener(this);
		menu2.add(listEtrMenuItem2);
		listEtrMenuItem2.addActionListener(this);
		menuBar.add(menu2);
		
		//Harmadik menu	
		JMenu menu3 = new JMenu(Labels.R_MENU_HARMADIK);
		menu3.add(addEtrMenuItem3);
		addEtrMenuItem3.addActionListener(this);
		menu3.add(listEtrMenuItem3);
		listEtrMenuItem3.addActionListener(this);
		menuBar.add(menu3);
		
		//Negyedik menu	
		JMenu menu4 = new JMenu(Labels.R_MENU_NEGYEDIK);
		menu4.add(addEtrMenuItem4);
		addEtrMenuItem4.addActionListener(this);
		menu4.add(listEtrMenuItem4);
		listEtrMenuItem4.addActionListener(this);
		menuBar.add(menu4);
		
		//Otodik menu	
		JMenu menu5 = new JMenu(Labels.R_MENU_OTODIK);
		menu5.add(addEtrMenuItem5);
		addEtrMenuItem5.addActionListener(this);
		menu5.add(listEtrMenuItem5);
		listEtrMenuItem5.addActionListener(this);
		menuBar.add(menu5);
	
		JMenu menu6 = new JMenu("Kijelentkez�s");
		menu6.add(kijelentkezes);
		kijelentkezes.setBackground( Color.GRAY );
		kijelentkezes.setForeground( Color.DARK_GRAY);
		kijelentkezes.addActionListener(this);
		menu6.addActionListener(this);
		menuBar.add(menu6);
		
		this.setJMenuBar(menuBar);
		setVisible(true);
	}
	
	public EtrController getController(){
		return controller;
	}
	


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == addEtrMenuItem1){
			new AddHallgatoDialog(this);
		} else if(e.getSource() == this.listEtrMenuItem1){
	
			String[][] sor= new String[50][10];
			String[] oszlop = {"Hallgat� EHA","Hallgat� neve","Hallgat� jelszava","Kezd�si �v",
					"Jelenlegi kreditsz�m","Megszerzett kreditsz�m","�vfolyam", "Tartoz�s"};
			int i = 0;
			for(Hallgato hallgato : controller.listHallgato()){
			/*	System.out.println(hallgato.getH_eha() + " " + hallgato.getH_nev()
				+ " " + hallgato.getH_jelszo() + " " + hallgato.getKezdesi_ev()
				 + " " + hallgato.getAkt_kreditszam() + " " + hallgato.getTelj_kreditszam()
				 + " " + hallgato.getEvfolyam());
			*/			
				
				String[] hozza = new String[8] ;
				String h_eha = hallgato.getH_eha();
				String h_nev = hallgato.getH_nev();
				String h_jelszo = hallgato.getH_jelszo();
				String kezdesi_ev = Integer.toString(hallgato.getKezdesi_ev());
				String akt_kreditszam = Integer.toString(hallgato.getAkt_kreditszam());
				String telj_kreditszam = Integer.toString(hallgato.getTelj_kreditszam());
				String evfolyam = Integer.toString(hallgato.getEvfolyam());
				String tartozas = Integer.toString(hallgato.getTartozas());
								
			
				hozza[0] = h_eha;
				hozza[1] = h_nev;
				hozza[2] = h_jelszo;
				hozza[3] = kezdesi_ev;
				hozza[4] = akt_kreditszam;
				hozza[5] = telj_kreditszam;
				hozza[6] = evfolyam;
				hozza[7] = tartozas;
				
				for(int j=0;j<8;j++){
					sor[i][j]= hozza[j];
				}
				i++;
						
			}
			
			JFrame frame = new JFrame();
		    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    
			JTable table = new JTable(sor,oszlop);
			
			 JScrollPane scrollPane = new JScrollPane(table);
			    frame.add(scrollPane, BorderLayout.CENTER);
			    frame.setTitle("Hallgat�k list�ja");
			    frame.setSize(750, 500);
			    frame.setLocation(300, 130);
			    frame.setVisible(true);
			
		}
		if(e.getSource() == addEtrMenuItem2){
			new AddOktatoDialog(this);
		} else if(e.getSource() == this.listEtrMenuItem2){
			
			String[][] sor= new String[50][10];
			String[] oszlop = {"Oktat� EHA","Oktat� neve","Oktat� jelszava"};
			int i = 0;
			
			for(Oktato oktato : controller.listOktato()){		
				/*System.out.println(oktato.getO_eha() + " " + oktato.getO_nev()
				+ " " + oktato.getO_jelszo());*/
				
				String[] hozza = new String[3] ;
				String o_eha = oktato.getO_eha();
				String o_nev = oktato.getO_nev();
				String o_jelszo = oktato.getO_jelszo();								
			
				hozza[0] = o_eha;
				hozza[1] = o_nev;
				hozza[2] = o_jelszo;
				
				for(int j=0;j<3;j++){
					sor[i][j]= hozza[j];
				}
				i++;
						
			}
			
			JFrame frame = new JFrame();
		    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    
			JTable table = new JTable(sor,oszlop);
			
			 JScrollPane scrollPane = new JScrollPane(table);
			    frame.add(scrollPane, BorderLayout.CENTER);
			    frame.setTitle("Oktat�k list�ja");
			    frame.setSize(750, 500);
			    frame.setLocation(300, 130);
			    frame.setVisible(true);
			
		}
		if(e.getSource() == addEtrMenuItem3){
			new AddKurzusDialog(this);
		} else if(e.getSource() == this.listEtrMenuItem3){
			
			String[][] sor= new String[50][10];
			String[] oszlop = {"Kurzusk�d","Oktat� EHA","Teremk�d","Id�pont","L�tsz�m","Kredit"};
			int i = 0;
			for(Kurzus kurzus : controller.listKurzus()){	
				
				String[] hozza = new String[6] ;
				String kurzuskod = kurzus.getKurzuskod();
				String o_eha = kurzus.getO_eha();
				String teremkod = kurzus.getTeremkod();
				String idopont = kurzus.getIdopont();
				String letszam = Integer.toString(kurzus.getLetszam());	
				String kredit = Integer.toString(kurzus.getKredit());	
			
				hozza[0] = kurzuskod;
				hozza[1] = o_eha;
				hozza[2] = teremkod;
				hozza[3] = idopont;
				hozza[4] = letszam;
				hozza[5] = kredit;
				
				for(int j=0;j<6;j++){
					sor[i][j]= hozza[j];
				}
				i++;
						
			}
			
			JFrame frame = new JFrame();
		    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    
			JTable table = new JTable(sor,oszlop);
			
			 JScrollPane scrollPane = new JScrollPane(table);
			    frame.add(scrollPane, BorderLayout.CENTER);
			    frame.setTitle("Kurzusok list�ja");
			    frame.setSize(750, 500);
			    frame.setLocation(300, 130);
			    frame.setVisible(true);
			
		}
		if(e.getSource() == addEtrMenuItem4){
			new AddTeremDialog(this);
		} else if(e.getSource() == this.listEtrMenuItem4){
			
			String[][] sor= new String[50][10];
			String[] oszlop = {"Teremk�d","Ep�let neve","�p�let c�me","F�r�hely"};
			int i = 0;
			for(Terem terem : controller.listTerem()){		
				
				String[] hozza = new String[4] ;
				String teremkod = terem.getTeremkod();
				String epulet_neve = terem.getEpulet_neve();
				String epulet_cime = terem.getEpulet_cime();
				String ferohely = Integer.toString(terem.getFerohely());					
			
				hozza[0] = teremkod;
				hozza[1] = epulet_neve;
				hozza[2] = epulet_cime;
				hozza[3] = ferohely;
				
				for(int j=0;j<4;j++){
					sor[i][j]= hozza[j];
				}
				i++;
						
			}
			
			JFrame frame = new JFrame();
		    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    
			JTable table = new JTable(sor,oszlop);
			
			 JScrollPane scrollPane = new JScrollPane(table);
			    frame.add(scrollPane, BorderLayout.CENTER);
			    frame.setTitle("Termek list�ja");
			    frame.setSize(750, 500);
			    frame.setLocation(300, 130);
			    frame.setVisible(true);
			
		}
		if(e.getSource() == addEtrMenuItem5){
			new AddVizsgaDialog(this);
		} else if(e.getSource() == this.listEtrMenuItem5){
			
			String[][] sor= new String[50][10];
			String[] oszlop = {"Azonos�t�","�raazonos�t�",
					"Jegy"};
			int i = 0;
			for(Vizsga vizsga : controller.listVizsga()){
			
				String[] hozza = new String[5] ;
				String az = Integer.toString(vizsga.getAz());
				String oaz = Integer.toString(vizsga.getOaz());
				String jegy = Integer.toString(vizsga.getJegy());
			
				hozza[0] = az;
				hozza[1] = oaz;
				hozza[2] = jegy;
				
				for(int j=0;j<3;j++){
					sor[i][j]= hozza[j];
				}
				i++;
						
			}
			
			JFrame frame = new JFrame();
		    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    
			JTable table = new JTable(sor,oszlop);
			
			 JScrollPane scrollPane = new JScrollPane(table);
			    frame.add(scrollPane, BorderLayout.CENTER);
			    frame.setTitle("Vizsg�k list�ja");
			    frame.setSize(750, 500);
			    frame.setLocation(300, 130);
			    frame.setVisible(true);
			
		}
		if(e.getSource() == kijelentkezes){
			controller.etrBelep();
			dispose();
		}
	}

	
}
