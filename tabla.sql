CREATE TABLE Hallgato (
	h_eha CHAR(11) NOT NULL,    
  h_nev VARCHAR2(50) NOT NULL, 
  h_jelszo VARCHAR2(50) NOT NULL,
	kezdesi_ev NUMBER,
	akt_kreditszam NUMBER,
  telj_kredit NUMBER,
  evfolyam NUMBER(2),            
	PRIMARY KEY(h_eha)         
);
CREATE TABLE Oktato (
	o_eha CHAR(11) NOT NULL,    
  o_nev VARCHAR2(50), 
  o_jelszo VARCHAR2(50),
	PRIMARY KEY(o_eha)         
);
CREATE TABLE Termek(
  teremkod CHAR(7),
  epulet_neve VARCHAR2(20),
  epulet_cime VARCHAR2(50),
  ferohely NUMBER(3),
  PRIMARY KEY(teremkod)
);
CREATE TABLE Kurzus (
  kurzuskod VARCHAR2 (10) NOT NULL,
  teremkod CHAR(7),
  idopont VARCHAR2(10),
  letszam NUMBER,
  kredit NUMBER,
  PRIMARY KEY(kurzuskod),
  FOREIGN KEY(teremkod) REFERENCES Termek
);
CREATE TABLE Ora(
  az NUMBER,
  kurzuskod VARCHAR2(10) NOT NULL,
  o_eha CHAR(11) NOT NULL,
  h_eha CHAR(11) NOT NULL,
  PRIMARY KEY(az),
  FOREIGN KEY(kurzuskod) REFERENCES Kurzus,
  FOREIGN KEY(o_eha) REFERENCES Oktato,
  FOREIGN KEY(h_eha) REFERENCES Hallgato
);
CREATE TABLE Vizsga(
  az NUMBER,
  kurzuskod VARCHAR2(10) NOT NULL,
  o_eha CHAR(11) NOT NULL,
  h_eha CHAR(11) NOT NULL,
  jegy NUMBER(1),
  PRIMARY KEY(az),
  FOREIGN KEY(kurzuskod) REFERENCES Kurzus,
  FOREIGN KEY(o_eha) REFERENCES Oktato,
  FOREIGN KEY(h_eha) REFERENCES Hallgato
);

-------------------------------------------------------------------
--Hallgat�k(30)
INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAA.SZE', 'Pelda Hallgato1', 'egyes', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAB.SZE', 'Pelda Hallgato2', 'kettes', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAC.SZE', 'Pelda Hallgato3', 'harmas', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAD.SZE', 'Pelda Hallgato4', 'negyes', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAE.SZE', 'Pelda Hallgato5', 'otos', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAF.SZE', 'Pelda Hallgato6', 'hatos', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAG.SZE', 'Pelda Hallgato7', 'hetes', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAH.SZE', 'Pelda Hallgato8', 'nyolcas', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAI.SZE', 'Pelda Hallgato9', 'kilences', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAAAJ.SZE', 'Pelda Hallgato10', 'tizes', '2016', 0, 0, 1);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABA.SZE', 'Pelda Hallgato11', 'egyes', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABB.SZE', 'Pelda Hallgato12', 'kettes', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABC.SZE', 'Pelda Hallgato13', 'harmas', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABD.SZE', 'Pelda Hallgato14', 'negyes', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABE.SZE', 'Pelda Hallgato15', 'otos', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABF.SZE', 'Pelda Hallgato16', 'hatos', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABG.SZE', 'Pelda Hallgato17', 'hetes', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABH.SZE', 'Pelda Hallgato18', 'nyolcas', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABI.SZE', 'Pelda Hallgato19', 'kilences', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAABJ.SZE', 'Pelda Hallgato20', 'tizes', '2015', 0, 30, 2);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACA.SZE', 'Pelda Hallgato21', 'egyes', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACB.SZE', 'Pelda Hallgato22', 'kettes', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACC.SZE', 'Pelda Hallgato23', 'harmas', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACD.SZE', 'Pelda Hallgato24', 'negyes', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACE.SZE', 'Pelda Hallgato25', 'otos', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACF.SZE', 'Pelda Hallgato26', 'hatos', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACG.SZE', 'Pelda Hallgato27', 'hetes', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACH.SZE', 'Pelda Hallgato28', 'nyolcas', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACI.SZE', 'Pelda Hallgato29', 'kilences', '2014', 0, 60, 3);

INSERT INTO Hallgato (h_eha, h_nev, h_jelszo, kezdesi_ev, akt_kreditszam, telj_kredit, evfolyam) 
VALUES ('AAAAACJ.SZE', 'Pelda Hallgato30', 'tizes', '2014', 0, 60, 3);
-------------------------------------------------------------------------------------
--Oktat�k(10)
INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAA.SZE', 'Pelda Oktato1', 'egyes');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAB.SZE', 'Pelda Oktato2', 'kettes');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAC.SZE', 'Pelda Oktato3', 'harmas');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAD.SZE', 'Pelda Oktato4', 'negyes');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAE.SZE', 'Pelda Oktato5', 'otos');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAF.SZE', 'Pelda Oktato6', 'hatos');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAG.SZE', 'Pelda Oktato7', 'hetes');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAH.SZE', 'Pelda Oktato8', 'nyolcas');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAI.SZE', 'Pelda Oktato9', 'kilences');

INSERT INTO Oktato (o_eha, o_nev, o_jelszo)
VALUES ('BBBBBAJ.SZE', 'Pelda Oktato10', 'tizes');
------------------------------------------------------------
--TERMEK(16)
INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('PPP-001', 'P epulet', 'P ter 1.', 10);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('PPP-002', 'P epulet', 'P ter 1.', 10);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('PPP-003', 'P epulet', 'P ter 1.', 10);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('PPP-004', 'P epulet', 'P ter 1.', 10);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('QQQ-001', 'Q epulet', 'Q ter 1.', 20);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('QQQ-002', 'Q epulet', 'Q ter 1.', 20);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('QQQ-003', 'Q epulet', 'Q ter 1.', 20);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('QQQ-004', 'Q epulet', 'Q ter 1.', 20);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('RRR-001', 'R epulet', 'R ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('RRR-002', 'R epulet', 'R ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('RRR-003', 'R epulet', 'R ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('RRR-004', 'R epulet', 'R ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('SSS-001', 'S epulet', 'S ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('SSS-002', 'S epulet', 'S ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('SSS-003', 'S epulet', 'S ter 1.', 30);

INSERT INTO Termek(teremkod,  epulet_neve, epulet_cime, ferohely)
VALUES('SSS-004', 'S epulet', 'S ter 1.', 30);
------------------------------------------------------------
--Kurzusok(10)
INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-001', 'SSS-001', 'H:10-12', 10, 1);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-002', 'SSS-002', 'H:10-12', 0, 2);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-003', 'SSS-001', 'H:12-14', 0, 3);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-004', 'SSS-002', 'H:12-14', 0, 4);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-005', 'PPP-002', 'H:10-12', 0, 5);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-006', 'RRR-002', 'H:10-12', 0, 6);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-007', 'QQQ-002', 'H:10-12', 0, 7);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-008', 'SSS-003', 'H:14-18', 0, 8);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-009', 'SSS-004', 'H:18-20', 0, 9);

INSERT INTO Kurzus(kurzuskod, teremkod, idopont, letszam, kredit)
VALUES('XXX-010', 'RRR-003', 'K:10-12', 0, 10);
--------------------------------------------------------------
--Ora(30)
INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(1, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAA.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(2, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAB.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(3, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAC.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(4, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAD.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(5, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAE.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(6, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAF.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(7, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAG.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(8, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAH.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(9, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAI.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(10, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAJ.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(11, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABA.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(12, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABB.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(13, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABC.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(14, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABD.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(15, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABE.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(16, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABF.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(17, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABG.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(18, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABH.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(19, 'XXX-002', 'BBBBBAB.SZE', 'AAAAABI.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(20, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABJ.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(21, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABA.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(22, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABB.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(23, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABC.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(24, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABD.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(25, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABE.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(26, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABF.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(27, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABG.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(28, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABH.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(29, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABI.SZE');

INSERT INTO Ora(az, kurzuskod, o_eha, h_eha)
VALUES(30, 'XXX-010', 'BBBBBAB.SZE', 'AAAAABJ.SZE');
-----------------------------------------------------------------
--Vizsga(4)
INSERT INTO Vizsga(az, kurzuskod, o_eha, h_eha, jegy)
VALUES(1, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAA.SZE', 5);

INSERT INTO Vizsga(az, kurzuskod, o_eha, h_eha, jegy)
VALUES(2, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAB.SZE', 1);

INSERT INTO Vizsga(az, kurzuskod, o_eha, h_eha, jegy)
VALUES(3, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAC.SZE', 3);

INSERT INTO Vizsga(az, kurzuskod, o_eha, h_eha, jegy)
VALUES(4, 'XXX-001', 'BBBBBAA.SZE', 'AAAAAAD.SZE', 4);



DROP TABLE Ora;
DROP TABLE Vizsga;
DROP TABLE Hallgato;
DROP TABLE Oktato;
DROP TABLE Kurzus;
DROP TABLE Termek;